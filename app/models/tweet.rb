class Tweet < ActiveRecord::Base
  default_scope -> { order(:created_at) }

  def self.fetch_tweets
    Twitter.user_timeline("DivuxVe", count: 3).each do |t|
      Tweet.where(tweet_id: t.id).first_or_create do |tweet|
        tweet.screen_name = t.user.screen_name
        tweet.content    = t.text
        tweet.tweet_id   = t.id
        tweet.created_at = t.created_at
      end
    end
  end
end
