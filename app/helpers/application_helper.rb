module ApplicationHelper
  def menu_item_class(item)
    'active' if controller_name == item.to_s.pluralize || controller_name == item.to_s
  end
end
