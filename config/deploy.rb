require "bundler/capistrano"
require "puma/capistrano"

set :application, "web"
set :repository,  "git@git.divux.com:divux/web.git"

server "windu.divux.com", :web, :app, :db, :primary => true

set :deploy_to,  "/home/divux/apps/#{application}"
set :deploy_via, :rsync_with_remote_cache

set :user, "divux"
set :use_sudo, false

set :default_environment, {
  'PATH' => "$HOME/.rbenv/shims:$HOME/.rbenv/bin:$PATH"
}

