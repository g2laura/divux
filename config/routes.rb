Web::Application.routes.draw do
  get "jobs/index"
  get "portfolio/chacao"
  get "portfolio/cnti"
  get "portfolio/shick"
  get "portfolio/show"
  get "contact/index"
  devise_for :users

  namespace :admin do
    resources :jobs
  end

  resources :services, only: [:index]
  resources :products, only: [:index]
  resources :portfolio, controller: 'portfolio'
  resource  :company

  root 'welcome#index'
end
